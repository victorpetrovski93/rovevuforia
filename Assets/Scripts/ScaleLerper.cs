﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleLerper : MonoBehaviour {
    Vector3 minScale;
    private Vector3 maxScale;
    public bool repeatable;
    public float speed = 2f;
    public float duration = 2f;

	// Use this for initialization
	IEnumerator Start () {
        Debug.Log("Max Scale:");
        Debug.Log(maxScale);
        maxScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
        Debug.Log(maxScale);

        minScale = new Vector3(0,0,0);
        while (repeatable)
        {
            //yield return RepeatLerp(minScale, maxScale, duration);

            yield return RepeatLerp(maxScale, minScale, duration);
        }
    }

    public IEnumerator RepeatLerp(Vector3 a, Vector3 b, float time)
    {
        float i = 0.0f;
        float rate = (1.0f / time) * speed;
        while(i < 1.0f)
        {
            i += Time.deltaTime * rate;
            transform.localScale = Vector3.Lerp(a, b, i);
            yield return null;
        }
    }

}
