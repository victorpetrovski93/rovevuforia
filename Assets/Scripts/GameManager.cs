﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField]
    private GameObject[] winObjects;
    [SerializeField]
    private GameObject[] loseObjects;
    [SerializeField]
    private GameObject[] gameObjects;

    public bool winningBox = true;

    public static GameManager Instance;

    private void Awake()
    {
        Instance = this;
        Init();
    }

    private void Init()
    {
        foreach (var item in winObjects) item.SetActive(false);
        foreach (var item in loseObjects) item.SetActive(false);
        foreach (var item in gameObjects) item.SetActive(false);
    }

    public void InitGame()
    {
        foreach (var item in gameObjects) item.SetActive(true);
    }

    public void GetEndResult()
    {
        if (winningBox) Win();
        else Lose();
    }

    private void Win()
    {
        AudioHandler.Instance.PlaySpecialCase("Win");
        AudioHandler.Instance.PlaySpecialCase("Cheer");
        foreach (var item in winObjects) item.SetActive(true);
        foreach (var item in gameObjects) item.SetActive(false);
    }

    private void Lose()
    {
        AudioHandler.Instance.PlaySpecialCase("Lose");
        AudioHandler.Instance.PlaySpecialCase("Disappointed");
        foreach (var item in loseObjects) item.SetActive(true);
        foreach (var item in gameObjects) item.SetActive(false);
    }

}
