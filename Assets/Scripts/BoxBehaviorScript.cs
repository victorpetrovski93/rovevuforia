﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxBehaviorScript : MonoBehaviour
{

    private ParticleSystem[] particles;

    private Animator animator;
    private AudioData currentAudioData = null;

    float startEmission, currentEmission = 45;
    float endEmission = 120;

    float startSize, currentSize = 15;
    float endSize = 100;

    float changeRatio = 2f;

    bool isSelecated = false;

    [SerializeField]
    private bool isMiddleBox;

    

    protected const string SHRINK_TRIGGER = "Shrink";

    private void Awake()
    {
        animator = GetComponent<Animator>();
        particles = GetComponentsInChildren<ParticleSystem>();
    }

    void Update()
    {
        if (isSelecated && (endSize > currentSize))
        {
            Debug.Log("Is selected");
            if(currentAudioData == null) currentAudioData = AudioHandler.Instance.PlaySpecialCase("DrumRoll");
            currentSize += changeRatio;
            currentEmission += changeRatio;

            foreach(var item in particles)
            {
                var emmisions = item.emission;
                emmisions.rateOverTime = currentEmission;
                ParticleSystem.MainModule main = item.main;
                main.startSize = currentSize;
            }
        }
        else if (isSelecated && currentSize >= endSize && currentAudioData != null && !currentAudioData.source.isPlaying)
        {
            
            ShowEndResult();
            currentAudioData = null;
            isSelecated = false;
        }
    }

    private void ShowEndResult()
    {
        if(!isMiddleBox) gameObject.GetComponent<MeshRenderer>().enabled = false;
        foreach (var item in particles)
        {
            var emmisions = item.emission;
            emmisions.rateOverTime = 0;
            ParticleSystem.MainModule main = item.main;
            main.startSize = 0;
        }
        GameManager.Instance.GetEndResult();
    }

    public void dismissBox()
    {
        animator.SetTrigger(SHRINK_TRIGGER);
        StartCoroutine(ShrinkBox());
    }

    public void selectBox()
    {
        isSelecated = true;
    }

    public float speed = 2f;
    public float duration = 2f;

    private IEnumerator ShrinkBox()
    {
        while (transform.localScale.x > .5f) yield return null;
        StartCoroutine(DestroyObject());
    }

    public IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(1);
        this.gameObject.SetActive(false);
    }



}
