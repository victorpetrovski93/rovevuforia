﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : MonoBehaviour
{
    [SerializeField]
    private AudioManager audioManager;

    private AudioData[] audioData;

    public static AudioHandler Instance;

    private void Awake()
    {
        Instance = this;
        audioData = audioManager.SceneAudioData;
    }

    public void StopBackgroundMusic()
    {
        foreach (var item in audioData)
        {
            if (item.audioType == AudioData.SceneAudioType.Background)
            {
                item.source.Stop();
            }
        }
    }

    public AudioData PlaySpecialCase(string name)
    {
        AudioData data = null;
        foreach (var item in audioData)
        {
            if (item.audioType != AudioData.SceneAudioType.SpecialCase || !item.name.Equals(name) || item.source.isPlaying) continue;
            item.source.Play();
            data = item;
            break;
        }
        return data;
    }

}
