﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreathEffect : MonoBehaviour
{
    [SerializeField]
    private Vector3 expandSize;
    [SerializeField]
    private Vector3 contractSize;
    [SerializeField]
    private float scaleSpeed;
    [Tooltip("Set true if to expand on start or false to contract on start")]
    [SerializeField]
    private bool startExpanding = true;

    private bool isExpanding;
    private float startTime;
    private float expandDistance;
    private Vector3 startingScale;
    private Vector3 targetScale;

    private void Start()
    {
        isExpanding = startExpanding;
        startTime = Time.time;
        startingScale = transform.localScale;
        if (startExpanding)
        {
            targetScale = expandSize;
            expandDistance = Vector3.Distance(transform.localScale, expandSize);
        }
        else
        {
            targetScale = contractSize;
            expandDistance = Vector3.Distance(transform.localScale, contractSize);
        }
    }

    private void Update()
    {
        if (isExpanding && Mathf.Approximately(Vector3.Distance(transform.localScale, expandSize), 0))
        {
            isExpanding = false;
            targetScale = contractSize;
            startingScale = transform.localScale;
            expandDistance = Vector3.Distance(transform.localScale, contractSize);
            startTime = Time.time;
        }
        else if (!isExpanding && Mathf.Approximately(Vector3.Distance(transform.localScale, contractSize), 0))
        {
            isExpanding = true;
            targetScale = expandSize;
            startingScale = transform.localScale;
            expandDistance = Vector3.Distance(transform.localScale, expandSize);
            startTime = Time.time;
        }
        float distCovered = (Time.time - startTime) * scaleSpeed;
        float fracDist = distCovered / expandDistance;
        transform.localScale = Vector3.Lerp(startingScale, targetScale, fracDist);
    }
}
