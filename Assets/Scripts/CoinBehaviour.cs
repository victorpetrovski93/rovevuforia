﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinBehaviour : MonoBehaviour
{

    [SerializeField]
    private float movementSpeedMin;
    [SerializeField]
    private float movementSpeedMax;

    private GameObject target;
    private float startTime;
    private float startingDistance;
    private float movementSpeed;
    private Vector3 startingPosition;

    private void Awake()
    {
        movementSpeed = Random.Range(movementSpeedMin, movementSpeedMax);
    }

    public void Update()
    {
        if (target)
        {
            if (Mathf.Approximately(Vector3.Distance(transform.position, target.transform.position), 0))
                ReachedTarget();
            float distCovered = (Time.time - startTime) * movementSpeed;
            float fracDist = distCovered / startingDistance;
            transform.position = Vector3.Lerp(startingPosition, target.transform.position, fracDist);
        }
    }

    public void SetTarget(GameObject targetObject)
    {
        target = targetObject;
        startTime = Time.time;
        startingDistance = Vector3.Distance(transform.position, target.transform.position);
        startingPosition = transform.position;
    }

    private void ReachedTarget()
    {
        TriggerReactor reactor = target.GetComponent<TriggerReactor>();
        if (reactor) reactor.HitReaction();
        AudioHandler.Instance.PlaySpecialCase("Coin");
        Destroy(gameObject);
    }

}
