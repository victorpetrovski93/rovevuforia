﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerReactor : MonoBehaviour
{

    [SerializeField]
    private float scaleAmount;
    [SerializeField]
    private float scaleTime;

    private int numberOfHits = 0;
    private int totalNumberOfHits = 0;
    private bool isScaled = false;
    private Vector3 startingScale;
    [SerializeField]
    private Text txt;

    private void Awake()
    {
        startingScale = transform.localScale;
        txt.text = "+" + totalNumberOfHits + "$";
    }

    public void HitReaction()
    {
        numberOfHits++;
        totalNumberOfHits++;
        txt.text = "+" + totalNumberOfHits + "$";
        if (!isScaled)
        {
            ScaleUp();
        }
        Invoke("LateReaction", scaleTime);
    }

    private void LateReaction()
    {
        if (numberOfHits == 1) ScaleDown();
        numberOfHits--;
    }

    private void ScaleUp()
    {
        Vector3 tempScale = startingScale;
        tempScale *= scaleAmount;
        transform.localScale = tempScale;
    }

    private void ScaleDown()
    {
        transform.localScale = startingScale;
    }
}
