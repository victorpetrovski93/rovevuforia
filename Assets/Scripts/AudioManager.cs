﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    [SerializeField]
    private AudioData[] sceneAudioData;
    public AudioData[] SceneAudioData { get { return sceneAudioData; } }
}

[Serializable]
public class AudioData
{
    public AudioSource source;
    public string name;
    public SceneAudioType audioType;

    public enum SceneAudioType
    {
        Background,
        SpecialCase
    }
}
