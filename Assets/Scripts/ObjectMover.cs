﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMover : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed;
    [SerializeField]
    private Vector3 moveDirection;

    private void Update()
    {
        Vector3 position = transform.localPosition;
        position += (moveDirection * moveSpeed * Time.deltaTime);
        transform.localPosition = position;
    }

}
