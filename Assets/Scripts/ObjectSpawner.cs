﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{

    [SerializeField]
    private GameObject objectToSpawn;
    [SerializeField]
    private GameObject target;
    [SerializeField]
    private float minSpawnTime;
    [SerializeField]
    private float maxSpawnTime;
    [SerializeField]
    private float startSpawnTime;
    [SerializeField]
    private float maxSpawns;

    private float timer = 0;
    private float spawnCount = 0;
    private float spawnInterval;

    void Start()
    {
        spawnInterval = startSpawnTime;
    }

    void Update()
    {
        if (spawnCount < maxSpawns)
        {
            timer += Time.deltaTime;
            if (timer >= spawnInterval)
            {
                GameObject temp = Instantiate(objectToSpawn);
                temp.transform.parent = transform;
                temp.transform.localPosition = Vector3.zero;
                spawnInterval = Random.Range(minSpawnTime, maxSpawnTime);
                spawnCount++;
                CoinBehaviour coinBehaviour = temp.GetComponent<CoinBehaviour>();
                if (coinBehaviour && target) coinBehaviour.SetTarget(target);
                timer = 0;
            }
        }
    }
}
