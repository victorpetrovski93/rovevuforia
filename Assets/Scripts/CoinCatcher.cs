﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCatcher : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Hit: " + other.name);
        if (other.tag.Equals("Coin"))
        {
            AudioHandler.Instance.PlaySpecialCase("CoinDrop");
            Destroy(other.gameObject);
        }
    }

}
