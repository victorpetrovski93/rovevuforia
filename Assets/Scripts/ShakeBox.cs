﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeBox : MonoBehaviour {

    private bool shaking = true;

    [SerializeField]
    private float shakeAmmount;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(shaking)
        {
            Shake();    
        }
    }

    public void Shake()
    {
        StartCoroutine("ShakeNow");
    }

    IEnumerator ShakeNow()
    {
        Vector3 originalPosition = transform.position;

        if( shaking == false)
        {
            shaking = true;
        }

        Vector3 newPosition = Random.insideUnitSphere * (Time.deltaTime * shakeAmmount);
        newPosition.y = transform.position.y;
        newPosition.z = transform.position.z;


        transform.position = Vector3.Lerp(originalPosition, newPosition, 1);

        yield return new WaitForSeconds(5f);

        //shaking = false;
        //transform.position = originalPosition;

    }
}
