﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoveBoxesHolderScript : MonoBehaviour
{

    public GameObject mRoveBoxMiddle;

    BoxBehaviorScript middleBoxScript;


    public GameObject mRoveBoxRight;

    BoxBehaviorScript rightBoxScript;


    public GameObject mRoveBoxLeft;

    BoxBehaviorScript leftBoxScript;

    
    void Start()
    {
        leftBoxScript = mRoveBoxLeft.GetComponent<BoxBehaviorScript>();
        middleBoxScript = mRoveBoxMiddle.GetComponent<BoxBehaviorScript>();
        rightBoxScript = mRoveBoxRight.GetComponent<BoxBehaviorScript>();

    }
    
    void Update()
    {
        InputChecker();
#if UNITY_EDITOR
        EditorInputChecker();
#endif
    }

    private void InputChecker()
    {
        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(touch.position);
                RaycastCheck(ray);
            }
        }
    }

    private void EditorInputChecker()
    {
        if (Input.GetMouseButtonDown(0))
        {
           
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastCheck(ray);
        }
    }

    private void RaycastCheck(Ray ray)
    {
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 200))
        {
            if (hit.transform.tag == "rove_box_middle")
            {
                AudioHandler.Instance.StopBackgroundMusic();
                onMiddleBoxSelected();
            }
            else if (hit.transform.tag == "rove_box_right")
            {
                AudioHandler.Instance.StopBackgroundMusic();
                onRightBoxSelected();
            }
            else if (hit.transform.tag == "rove_box_left")
            {
                AudioHandler.Instance.StopBackgroundMusic();
                onLeftBoxSelected();
            }
        }
    } 

    private void onMiddleBoxSelected()
    {
        leftBoxScript.dismissBox();
        rightBoxScript.dismissBox();
        middleBoxScript.selectBox();
    }

    private void onRightBoxSelected()
    {
        leftBoxScript.dismissBox();
        middleBoxScript.dismissBox();
        rightBoxScript.selectBox();
    }

    private void onLeftBoxSelected()
    {
        middleBoxScript.dismissBox();
        rightBoxScript.dismissBox();
        leftBoxScript.selectBox();
    }

    private void _ShowAndroidToastMessage(string message)
    {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

        if (unityActivity != null)
        {
            AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
            unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                    message, 0);
                toastObject.Call("show");
            }));
        }
    }
}
