﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBox : MonoBehaviour {

	// Use this for initialization
	void Start () {
		rotationsPerMinute = Random.Range(20, 30);
    }

    float rotationsPerMinute = 0; 

	// Update is called once per frame
	void Update () {
        transform.Rotate(0, (float)(6.0 * rotationsPerMinute * Time.deltaTime), 0);
    }
}
